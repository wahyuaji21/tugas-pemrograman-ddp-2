package ddp2_tp1.gui;

import ddp2_tp1.core.Kurakura;

import java.awt.Dimension;
import java.awt.Graphics2D; 
import java.awt.geom.AffineTransform; 
import java.awt.image.BufferedImage;

/**
 * 
 * KurakuraGUI.java
 * 
 * Class {@code KurakuraGUI} mendefinisikan object kura-kura.
 * Object kura-kura digerakkan oleh beberapa method antara lain:
 * maju, mundur dan rotasi. Object kura-kura digambarkan oleh method
 * {@code draw} yang akan dipanggil oleh object yang memiliki 
 * object kura-kura ini untuk digambarkan pada object tersebut.
 * Object kura-kura ini direpresentasikan oleh image kura-kura. 
 * Informasi mengenai nama file image terletak pada 
 * text file dengan nama {@code kurakuraku.properties}.
 * File ber extensi {@code .properties} dapat diakses langsung 
 * melalui library java tertentu.
 * <br>
 * <br>
 * Tugas anda pada file ini:<br>
 * - Lengkapi javadoc comment pada tiap deklarasi method. <br>
 * - Lengkapi inline comment untuk tiap baris perintah yang penting.<br>
 * - Lengkapi method "mundur" yang belum ada implementasinya.<br> 
 * 
 * @author DPBO 2008 @ Fasilkom UI
 *
 */
public class KurakuraGUI {
    private Kurakura kurakura;
    private int width = 400, height = 300;  // default ukuran layar/canvas
    private double arah = 0; // arah 0 -> sumbu x
    private boolean jejak = true;    // status apakan membuat jejak atau tidak

    private String imageName = "turtle.gif"; // default file name
    private java.awt.Image img;                  // object image kura-kura.
    private BufferedImage imgJejak;      // object image untuk jejak kura-kura
    private AffineTransform matRotasi;  // mendefinisikan matriks rotasi
    private AffineTransform matTrans;  // mendefinisikan matriks translasi
    private AffineTransform matGabung;  // mendefinisikan matriks gabungan
   
    /** Creates a new instance of KurakuraGUI */
    public KurakuraGUI(){
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("Kurakuraku");
        imageName = "src/main/resources/" + bundle.getString("image");
        img = java.awt.Toolkit.getDefaultToolkit().getImage(imageName);

        matRotasi = new AffineTransform();
        matGabung = new AffineTransform();
        matTrans = new AffineTransform();

        this.kurakura = new Kurakura();
        kurakura.setKurakuraGUI(this);
        reset();
    }

    public void setKurakura(Kurakura kurakura){
        this.kurakura = kurakura;
        setPosition(kurakura.getPositionX(), kurakura.getPositionY());
    }

    public Kurakura getKurakura(){
        return kurakura;
    }
    
    public KurakuraGUI(int w, int h){
        this();
        setSize(w,h);
        reset();
    }
    
    public void setSize(int w, int h){        
        width = w;
        height = h;        
        imgJejak = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
    }
     
    public void reset(){        
        double x = width/2-15;
        double y = height/2-20;

        kurakura.setPosition(x, y);
    }

    public void drawLine(double x1, double y1, double x2, double y2, boolean jejak){
        if(jejak){
            Graphics2D g = imgJejak.createGraphics();
            g.draw(new java.awt.geom.Line2D.Double(x1, y1, x2, y2));
        }

        matTrans.setToTranslation(x2, y2);
    }

    public void drawLineWithDiff(double x, double y, double diffX, double diffY, boolean jejak){
        drawLine(x, y, x + diffX, y + diffY, jejak);
    }
    
    public void setJejak(boolean v){
        jejak = v;
    }

    public boolean getJejak(){
        return jejak;
    }
    
    public void draw(Graphics2D g) {
        matGabung.setToIdentity();
        matGabung.concatenate(matTrans);
        matGabung.concatenate(matRotasi);
        if (imgJejak != null){
            g.drawImage(imgJejak,img.getWidth(null)/2,img.getHeight(null)/2,null);
        }

        if (img != null){
            g.drawImage(img, matGabung, null);
        }
    }

    public void setPosition(double x, double y){
        matTrans.setToTranslation(x, y);
    }
    
    public double getArah(){
        return arah;
    }
    
    public void setArah(double arah){
        matRotasi.setToRotation(arah,img.getWidth(null)/2,img.getHeight(null)/2); // rotasi dihitung dari pusat image.              
    }
}
