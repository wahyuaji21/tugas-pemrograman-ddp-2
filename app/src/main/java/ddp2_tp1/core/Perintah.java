package ddp2_tp1.core;

import ddp2_tp1.gui.Canvas;
import ddp2_tp1.gui.KurakuraGUI;

import java.awt.Dimension;

/*
 * Perintah.java
 * <br><br>
 * Class {@Perintah} merepresentasikan perintah-perintah umum yang 
 * dapat diberikan kepada kura-kura. Termasuk dalam class ini adalah
 * proses untuk membaca input (saat ini baru melalui satu baris perintah)
 * dan memanggil method yang berkesesuaian.
 * Dalam kelas ini juga disediakan method-method yang merupakan kumpulan-kumpulan
 * perintah berurutan yang dapat diterima oleh kurakura dan menghasilkan gambar 
 * tertentu. 
 * <br><br>
 * Tugas anda pada file ini: <br>
 * - Lengkapi javadoc comment pada tiap deklarasi method.<br>
 * - Lengkapi inline comment untuk tiap baris perintah yang penting.<br>
 * - Lengkapi method spiral dan linkaran <br>
 * - Perbaiki dan lengkapi method {@code lakukan} agar menerima perintah <br>
 *   pembuatan spiral dan lingkaran, 
 * - Perbaiki method {@code lakukan} agar tidak menimbulkan error bila input salah<br>
 * - Dapatkan anda membuat kura-kura mengenali perintah "loop 10 perintah" <br>
 *   yang artinya melakukan perintah sebanyak 10 kali? <br>
 *   contoh: "loop 10 rotasi 10 maju 10" <br>
 *           yang artinya akan melakukan perintah "rotasi 10", dilanjutkan <br>
 *           perintah "maju 10", secara berulang-ulang sebanyak 10 kali<br>
 * 
 * 
 * @author DPBO 2008 @ Fasilkom UI
 * 
 */
public class Perintah {
    Canvas canvas;
    Kurakura kurakuraku;

    /** Creates a new instance of Perintah */
    public Perintah(KurakuraGUI k, Canvas canvas) {
        kurakuraku = k.getKurakura();
        this.canvas = canvas;
    }

    // Dapatkan anda membuat method ini lebih baik dan lebih mudah ditambahkan
    // atau di ubah?
    public String lakukan(String inputPerintah) {
        String[] in = inputPerintah.split(" ");
        if (in[0].equalsIgnoreCase("maju")) {
            double jarak = Integer.parseInt(in[1]);
            kurakuraku.maju(jarak);
        } else if (in[0].equalsIgnoreCase("mundur")) {
            double jarak = Integer.parseInt(in[1]);
            // TODO : Lengkapi bagian ini
        } else if (in[0].equalsIgnoreCase("rotasi")){
            double sudutRadian = Integer.parseInt(in[1]);
            kurakuraku.rotasiRadian(sudutRadian);
        } else if (in[0].equalsIgnoreCase("jejak")){
            boolean jejak = Boolean.parseBoolean(in[1]);
            // TODO : lengkapi bagian ini
        } else if (in[0].equalsIgnoreCase("pindah")){
            int koordinatX = Integer.parseInt(in[1]);
            int koordinatY = Integer.parseInt(in[1]);
            kurakuraku.setPosition(koordinatX, koordinatY);
        } else {
            canvas.repaint();
            return "Perintah tidak dipahami.";
        }

        canvas.repaint();
        return "Perintah sudah dilaksanakan.";
    }

    // TODO: Buat method buatPersegi(int panjangSisi)

    public void buatPohon(){
        boolean jejakInit = kurakuraku.getJejak();
        kurakuraku.setJejak(false);
        kurakuraku.reset();
        kurakuraku.rotasiRadian(Math.toRadians(90));
        kurakuraku.maju(100);
        kurakuraku.rotasiRadian(Math.toRadians(180));
        buatPohon(6, 50);
        kurakuraku.setJejak(jejakInit);
    }

    private void buatPohon(int ukuran, int tinggi) {
        if (ukuran > 0) {
            kurakuraku.setJejak(true);
            kurakuraku.maju(tinggi);
            kurakuraku.rotasiRadian(Math.toRadians(-45));;
            Dimension posAwal = kurakuraku.getPosition();
            double arah = kurakuraku.getArah();
            double sudut = arah;
            for (int i = 0; i < 3; i++) {
                buatPohon(ukuran - 1, (int) (tinggi / 1.5));
                kurakuraku.setJejak(false);
                kurakuraku.setPosition(posAwal.getWidth(), posAwal.getHeight());
                kurakuraku.setArah(arah);
                sudut += Math.toRadians(45);
                kurakuraku.rotasiRadian(sudut);
            }
        }

        kurakuraku.reset();
    }
}
